package main

import (
	"bitbucket.org/perfectgentlemande/go-one-more-jwt-example/awesomelog"
	"context"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func NewLoggingMiddleware(log *logrus.Entry) func(http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			nextEntry := log.WithFields(logrus.Fields{
				"method": r.Method,
				"path":   r.URL.Path,
			})

			handler.ServeHTTP(w, r.WithContext(awesomelog.ContextWithLogEntry(r.Context(), nextEntry)))
		})
	}
}

func main() {
	ctx := context.Background()

	log := logrus.New()
	log.SetFormatter(&logrus.JSONFormatter{})
	logEntry := logrus.NewEntry(log)
	ctx = awesomelog.ContextWithLogEntry(ctx, logEntry)

	r := chi.NewRouter()
	//r.With(NewLoggingMiddleware(logEntry)).With(basicAuthMiddleware).Get("/", greeting)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}
	defer srv.Close()

	stop := make(chan os.Signal, 4)
	signal.Notify(stop,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Fatal("start server")
		}
	}()

	<-stop
	log.Info("caught stop signal")

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.WithError(err).Fatal("Server Shutdown Failed")
	}

	log.Info("Server Exited Properly")
}

