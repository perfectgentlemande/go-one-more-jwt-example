module bitbucket.org/perfectgentlemande/go-one-more-jwt-example

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
