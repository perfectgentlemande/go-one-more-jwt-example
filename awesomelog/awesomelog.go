package awesomelog

import (
	"context"
	"github.com/sirupsen/logrus"
)

type loggerCtxKey struct{}
func ContextWithLogEntry(ctx context.Context, logEntry *logrus.Entry) context.Context {
	return context.WithValue(ctx, loggerCtxKey{}, logEntry)
}
func LogEntryFromContext(ctx context.Context) *logrus.Entry {
	le, ok := ctx.Value(loggerCtxKey{}).(*logrus.Entry)
	if !ok {
		le = logrus.NewEntry(logrus.New())
	}
	return le
}
